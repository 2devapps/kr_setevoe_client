package com.mobimore;

import java.io.File;

public class Configuration {
    public static final String SERVER_ADDRESS  = "127.0.0.1";
    public static final int SERVER_PORT = 1050;
    public static final String IMAGES_CACHE = "cache" + File.separator + "images";
}
