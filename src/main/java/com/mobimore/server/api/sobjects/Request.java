package com.mobimore.server.api.sobjects;

import java.util.HashMap;
import java.util.Map;

public class Request {
    private Map<String, String> requestBody = new HashMap<>();
    private String commandName;

    public Request(Map<String, String> requestBody, String commandName){
        this.requestBody = new HashMap<>(requestBody);
        this.commandName = commandName;
    }
    public Request(String message, String commandName) {
        requestBody.put("message", message);
        this.commandName = commandName;
    }
    public Request(String commandName){
        this.commandName = commandName;
    }

    public String getCommandName() {
        return commandName;
    }
    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public Map<String, String> getRequestBody() {
        return requestBody;
    }
    public void setRequestBody(Map<String, String> requestBody) {
        this.requestBody = new HashMap<>(requestBody);
    }

    public void addRequestEntry(String key, String value) {
        requestBody.put(key, value);
    }
    public String getRequestEntry(String key) {
        return requestBody.get(key);
    }
    public void removeRequestEntry(String key) {
        requestBody.remove(key);
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestBody=" + requestBody +
                ", commandName='" + commandName + '\'' +
                '}';
    }
}
