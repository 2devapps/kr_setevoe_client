package com.mobimore.server.api.sobjects;

import com.mobimore.server.api.ExecRank;

public class User {
	private int id;
	private String name;
	private String login;
	private String password;
	private ExecRank rank = ExecRank.NAU;
	private String authToken;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public ExecRank getRank() {
		return rank;
	}

	public void setRank(ExecRank rank) {
		this.rank = rank;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", login='" + login + '\'' +
				", password='" + password + '\'' +
				", rank=" + rank +
				", authToken='" + authToken + '\'' +
				'}';
	}
}
