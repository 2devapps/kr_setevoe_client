package com.mobimore.server.api.sobjects;

import java.util.HashMap;

public class Order {
    //Key - productID, Value - productCount in order
    private HashMap<Integer, Integer> productAndCountMap = new HashMap<>();
    private String deliveryAddress;
    private PaymentType paymentType;
    private String id;

    public void addProduct(Product product, int count) {
        productAndCountMap.put(product.getId(), count);
    }
    public void removeProduct(Product product) {
        productAndCountMap.remove(product.getId());
    }

    public HashMap<Integer, Integer> getProductAndCountMap() {
        return productAndCountMap;
    }
    public void setProductAndCountMap(HashMap<Integer, Integer> productAndCountMap) {
        this.productAndCountMap = productAndCountMap;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }
    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "productAndCountMap=" + productAndCountMap +
                ", deliveryAddress='" + deliveryAddress + '\'' +
                ", paymentType=" + paymentType +
                ", id='" + id + '\'' +
                '}';
    }

    public enum PaymentType{
        ONLINE, CASH
    }
}
