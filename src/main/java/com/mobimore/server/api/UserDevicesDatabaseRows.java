package com.mobimore.server.api;

public enum UserDevicesDatabaseRows {
	name,
	ip_addr,
	andr_ver,
	imei,
	registration_id,
	email_addr,
	hash,
	last_update
}
