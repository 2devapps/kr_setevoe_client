package com.mobimore.model;

import com.mobimore.server.api.sobjects.Product;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.util.Objects;

public class ProductWrapper {
    private SimpleObjectProperty<Product> product = new SimpleObjectProperty<>(this, "product");
    private SimpleIntegerProperty count = new SimpleIntegerProperty(this, "count");

    public ProductWrapper(Product product) {
        this.product.set(product);
        count.set(1);
    }

    public void incCount(){
        count.set(count.get() + 1);
    }
    public void decCount(){
        count.set(count.get() - 1);
    }

    public int getCount() {
        return count.get();
    }
    public SimpleIntegerProperty countProperty() {
        return count;
    }
    public void setCount(int count) {
        this.count.set(count);
    }

    public Product getProduct() {
        return product.get();
    }
    public SimpleObjectProperty<Product> productProperty() {
        return product;
    }
    public void setProduct(Product product) {
        this.product.set(product);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductWrapper that = (ProductWrapper) o;
        return product.equals(that.product) &&
                count.equals(that.count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, count);
    }
}
