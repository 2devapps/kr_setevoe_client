package com.mobimore.model;

import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class Cart {
    private ObservableList<ProductWrapper> productList = FXCollections.observableArrayList();

    private ReadOnlyIntegerWrapper productsCount = new ReadOnlyIntegerWrapper(this, "productsCount");
    private ReadOnlyIntegerWrapper totalPrice = new ReadOnlyIntegerWrapper(this, "totalPrice");
    private ReadOnlyBooleanWrapper empty = new ReadOnlyBooleanWrapper(this, "empty", true);

    public Cart() {
        /*productList.addListener((InvalidationListener) e->{
            productsCount.set(productList.size());
        });*/

        productList.addListener((ListChangeListener<? super ProductWrapper>) e->{
            e.next();
            empty.set(productList.isEmpty());
            if (e.wasAdded()) {
                for (ProductWrapper productWrapper : e.getAddedSubList()) {
                    productsCount.set(productsCount.get() + productWrapper.getCount());
                    totalPrice.set(totalPrice.get() + productWrapper.getCount() * productWrapper.getProduct().getPrice());
                }
            }

            if (e.wasRemoved()) {
                for (ProductWrapper productWrapper : e.getRemoved()) {
                    productsCount.set(productsCount.get() - productWrapper.getCount());
                    totalPrice.set(totalPrice.get() - productWrapper.getCount() * productWrapper.getProduct().getPrice());
                }
            }
        });
    }

    public void addToCart(ProductWrapper product) {
        if (!productList.contains(product)) {
            productList.add(product);
            product.countProperty().addListener(e->{
                productsCount.set(0);
                totalPrice.set(0);
                for (ProductWrapper productWrapper : productList) {
                    productsCount.set(productsCount.get() + productWrapper.getCount());
                    totalPrice.set(totalPrice.get() + (productWrapper.getCount() * productWrapper.getProduct().getPrice()));
                }
            });
        }else{
            product.incCount();
        }
    }
    public void removeFromCart(ProductWrapper product){
        if (product.getCount() == 1) {
            productList.remove(product);
        }else{
            product.decCount();
        }

    }

    public ObservableList<ProductWrapper> getProductList() {
        return productList;
    }

    public int getProductsCount() {
        return productsCount.get();
    }
    public ReadOnlyIntegerProperty productsCountProperty() {
        return productsCount.getReadOnlyProperty();
    }

    public int getTotalPrice() {
        return totalPrice.get();
    }
    public ReadOnlyIntegerWrapper totalPriceProperty() {
        return totalPrice;
    }

    public boolean isEmpty() {
        return empty.get();
    }
    public ReadOnlyBooleanProperty emptyProperty() {
        return empty.getReadOnlyProperty();
    }
}
