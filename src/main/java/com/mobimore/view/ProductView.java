package com.mobimore.view;

import com.mobimore.Configuration;
import com.mobimore.controller.ProductViewController;
import com.mobimore.server.api.sobjects.Product;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.File;
import java.io.IOException;

public class ProductView extends Pane {
    private Node view;
    private ProductViewController controller;
    private Product product;

    public ProductView(Product product) {
        this.product = product;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/productPreview.fxml"));
        fxmlLoader.setControllerFactory(param -> controller = new ProductViewController());
        try {
            view = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        getChildren().add(view);

        if (product != null) {
            setImage(new Image("file:" + Configuration.IMAGES_CACHE + File.separator + product.getType() + File.separator + product.getImageName()));
            setTitle(product.getName());
            setDescription(product.getDescription());
            setPrice(product.getPrice() + " rub");
        }
    }

    public String getTitle() {
        return controller.title.getText();
    }

    public void setTitle(String title) {
        controller.title.setText(title);
    }

    public String getDescription() {
        return controller.description.getText();
    }

    public void setDescription(String description) {
        controller.description.setText(description);
    }

    public String getPrice() {
        return controller.price.getText();
    }

    public void setPrice(String price) {
        controller.price.setText(price);
    }

    public Image getImage(){
        return controller.image.getImage();
    }

    public void setImage(Image image) {
        controller.image.setImage(image);
    }

    public void setAddToCartAction(EventHandler<? super MouseEvent> action) {
        controller.addToCart.setOnMouseClicked(action);
    }

    public Product getProduct() {
        return product;
    }
}
