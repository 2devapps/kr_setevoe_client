package com.mobimore.controller;

import com.google.gson.Gson;
import com.mobimore.Configuration;
import com.mobimore.utils.HTTPUtils;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.utils.FXUtils;
import com.victorlaerte.asynctask.AsyncTask;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;


public class LoginController {
    @FXML
    public Label registerButton;

    @FXML
    public TextField loginField;

    @FXML
    public TextField passwordField;

    @FXML
    public VBox progressIndicatorVBOX;

    @FXML
    public AnchorPane inputPane;

    @FXML
    public void initialize(){
        registerButton.setOnMouseClicked(event -> {
            FXUtils.showModalWindow("/fxml/registration.fxml", "Pizza system");
        });
    }

    @FXML
    public void loginButton(ActionEvent actionEvent) {
        String login = loginField.getText();
        String password = passwordField.getText();

        if (login == null || login.equals("")) {
            FXUtils.showAlert(Alert.AlertType.ERROR, "Log in error", "Login field empty");
            return;
        } else if (password == null || password.equals("")) {
            FXUtils.showAlert(Alert.AlertType.ERROR, "Log in error", "Password field empty");
            return;
        }

        AsyncTask<String, Object, JSONResponse> asyncTask = new HTTPLoginTask();
        asyncTask.execute(login, password);
    }

    private class HTTPLoginTask extends AsyncTask<String, Object, JSONResponse> {
        private Gson gson = new Gson();

        @Override
        public void onPreExecute() {
            inputPane.setDisable(true);
            progressIndicatorVBOX.setVisible(true);
        }

        @Override
        public JSONResponse doInBackground(String[] params) {
            try {
                String url = "http://" + Configuration.SERVER_ADDRESS + ":" + Configuration.SERVER_PORT + "/getToken";
                String urlParameters = "login="+params[0]+"&password="+params[1];
                String response = HTTPUtils.getInstance().makeHTTPpostRequest(url, urlParameters);
                return gson.fromJson(response, JSONResponse.class);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        public void onPostExecute(JSONResponse response) {
            progressIndicatorVBOX.setVisible(false);
            inputPane.setDisable(false);
            if (response != null) {
                if (response.getErrorCode() == 0) {
                    String token = response.getResponseEntry("token");
                    if (token != null && !token.equals("")) {
                        //FXUtils.showAlert(Alert.AlertType.INFORMATION, "Log in OK", "Token: " + token);
                        HTTPUtils.getInstance().setToken(token);
                        ((Stage) inputPane.getScene().getWindow()).close();
                        FXUtils.showWindow("/fxml/mainWindow.fxml", "Pizza system");
                        return;
                    } else {
                        FXUtils.showAlert(Alert.AlertType.ERROR, "Log in error", "Check your login and password");
                        return;
                    }
                }else {
                    FXUtils.showAlert(Alert.AlertType.ERROR, "Log in error", "Check your login and password");
                    return;
                }
            }
            FXUtils.showAlert(Alert.AlertType.ERROR, "Log in error", "Bad server response or server unavailable");
        }

        @Override
        public void progressCallback(Object[] objects) {

        }
    }
}
