package com.mobimore.controller;

import com.google.gson.Gson;
import com.mobimore.Configuration;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.utils.FXUtils;
import com.mobimore.utils.HTTPUtils;
import com.victorlaerte.asynctask.AsyncTask;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class RegistrationController {
    public TextField nameField;
    public TextField passwordField;
    public TextField loginField;
    public VBox progressIndicatorVBOX;

    public void onRegister(ActionEvent actionEvent) {
        String login = loginField.getText();
        String password = passwordField.getText();
        String userName = nameField.getText();

        if (login == null || login.equals("")) {
            FXUtils.showAlert(Alert.AlertType.ERROR, "Sign up error", "Login field empty");
            return;
        } else if (password == null || password.equals("")) {
            FXUtils.showAlert(Alert.AlertType.ERROR, "Sign up error", "Password field empty");
            return;
        }
        else if(userName == null || userName.equals("")){
            FXUtils.showAlert(Alert.AlertType.ERROR, "Sign up error", "Name field empty");
            return;
        }
        new RegisterTask().execute(login, password, userName);
    }

    private class RegisterTask extends AsyncTask<String, Object, JSONResponse> {
        private Gson gson = new Gson();

        @Override
        public void onPreExecute() {
            progressIndicatorVBOX.setVisible(true);
        }

        @Override
        public JSONResponse doInBackground(String[] params) {
            try {
                String url = "http://" + Configuration.SERVER_ADDRESS + ":" + Configuration.SERVER_PORT + "/register";
                String urlParameters = "login=" + params[0] + "&name=" + params[2] + "&password=" + params[1];
                String response = HTTPUtils.getInstance().makeHTTPpostRequest(url, urlParameters);
                return gson.fromJson(response, JSONResponse.class);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        public void onPostExecute(JSONResponse response) {
            progressIndicatorVBOX.setVisible(false);
            if (response != null) {
                if (response.getErrorCode() == 0) {
                    FXUtils.showAlert(Alert.AlertType.INFORMATION,"Sign up OK","You can log in now");
                } else {
                    FXUtils.showAlert(Alert.AlertType.ERROR, "Sign up error", response.getResponseEntry("message"));
                }
            } else {
                FXUtils.showAlert(Alert.AlertType.ERROR, "Sign up error", "Bad server response");
            }
        }


        @Override
        public void progressCallback(Object... objects) {

        }
    }
}
