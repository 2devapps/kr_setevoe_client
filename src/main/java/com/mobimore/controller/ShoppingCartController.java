package com.mobimore.controller;

import com.google.gson.Gson;
import com.mobimore.Configuration;
import com.mobimore.model.Cart;
import com.mobimore.model.ProductWrapper;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Order;
import com.mobimore.utils.FXUtils;
import com.mobimore.utils.HTTPUtils;
import com.victorlaerte.asynctask.AsyncTask;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;

public class ShoppingCartController {

    public Label itemsInShoppingCart;
    public Label totalPriceLabel;
    public TableColumn<ProductWrapper, Integer> numberColumn;
    public TableColumn<ProductWrapper, String> nameColumn;
    public TableColumn<ProductWrapper, Integer> countColumn;
    public TableColumn<ProductWrapper, Integer> priceColumn;
    public TableView<ProductWrapper> cartTableView;
    public TextField deliveryAddress;
    public RadioButton buttonPayOnline;
    public RadioButton buttonPayByCash;
    public Button okButton;
    public VBox progressIndicatorVBOX;

    private Cart cart;

    public void initialize(){
        numberColumn.setCellFactory(new LineNumbersCellFactory<>());
        nameColumn.setCellValueFactory(e -> new ReadOnlyStringWrapper(e.getValue().getProduct().getName()));
        priceColumn.setCellValueFactory(e -> new ReadOnlyObjectWrapper<>(e.getValue().getProduct().getPrice()));
        countColumn.setCellFactory(param -> new TableCell<ProductWrapper, Integer>() {
            private final Button removeButton = new Button("-");
            private final Button addButton = new Button("+");

            @Override
            protected void updateItem(Integer productWrapper, boolean empty) {
                super.updateItem(productWrapper, empty);
                if (empty) {
                    setGraphic(null);
                    return;
                }

                ProductWrapper getProductWrapper = getTableView().getItems().get(getIndex());

                Label count = new Label();
                count.textProperty().bind(getProductWrapper.countProperty().asString());
                addButton.setOnAction(event -> {
                    cart.addToCart(getProductWrapper);
                });

                removeButton.setOnAction(event -> {
                    cart.removeFromCart(getProductWrapper);
                });

                HBox hBox = new HBox(removeButton, count, addButton);
                hBox.setSpacing(10);
                hBox.setAlignment(Pos.CENTER);
                setGraphic(hBox);
            }
        });
    }

    public void closeShoppingCart(ActionEvent actionEvent) {
        ((Stage) itemsInShoppingCart.getScene().getWindow()).close();
    }

    public void onOkButtonPressed(ActionEvent actionEvent) {
        if (deliveryAddress.getText().isEmpty()) {
            FXUtils.showAlert(Alert.AlertType.ERROR,"Error!","Delivery address can't be empty!");
            return;
        }

        Order order = new Order();

        order.setDeliveryAddress(deliveryAddress.getText());

        cart.getProductList().forEach(e->{
            order.addProduct(e.getProduct(), e.getCount());
        });

        if (buttonPayOnline.isSelected()) {
            order.setPaymentType(Order.PaymentType.ONLINE);
        }else{
            order.setPaymentType(Order.PaymentType.CASH);
        }

        new PlaceOrderTask().execute(order);
    }

    public void setShoppingCart(Cart shoppingCart) {
        this.cart = shoppingCart;
        cartTableView.setItems(shoppingCart.getProductList());
        okButton.disableProperty().bind(shoppingCart.emptyProperty());
        itemsInShoppingCart.textProperty().bind(shoppingCart.productsCountProperty().asString());
        totalPriceLabel.textProperty().bind(shoppingCart.totalPriceProperty().asString());
    }

    private class PlaceOrderTask extends AsyncTask<Order, Object, JSONResponse> {
        private Gson gson = new Gson();

        @Override
        public void onPreExecute() {
            //inputPane.setDisable(true);
            progressIndicatorVBOX.setVisible(true);
        }

        @Override
        public JSONResponse doInBackground(Order[] params) {
            if (params.length == 0) {
                return null;
            }
            try {
                String url = "http://" + Configuration.SERVER_ADDRESS + ":" + Configuration.SERVER_PORT + "/placeOrder";
                String urlParameters = "order=" + gson.toJson(params[0]);
                String response = HTTPUtils.getInstance().makeHTTPpostRequest(url, urlParameters);
                return gson.fromJson(response, JSONResponse.class);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        public void onPostExecute(JSONResponse response) {
            progressIndicatorVBOX.setVisible(false);
            //inputPane.setDisable(false);
            if (response != null) {
                if (response.getErrorCode() == 0) {
                    FXUtils.showAlert(Alert.AlertType.INFORMATION, "Your order placed!", "Your order was placed successfully, now we are processing your order");
                    return;
                }else {
                    FXUtils.showAlert(Alert.AlertType.ERROR, "Error!", "Can't place your order now");
                    return;
                }
            }
            FXUtils.showAlert(Alert.AlertType.ERROR, "Error!", "Bad server response or server unavailable");
        }

        @Override
        public void progressCallback(Object[] objects) {

        }
    }

    private class LineNumbersCellFactory<T, E> implements Callback<TableColumn<T, E>, TableCell<T, E>> {

        LineNumbersCellFactory() {
        }

        @Override
        public TableCell<T, E> call(TableColumn<T, E> param) {
            return new TableCell<T, E>() {
                @Override
                protected void updateItem(E item, boolean empty) {
                    super.updateItem(item, empty);

                    if (!empty) {
                        setText(this.getTableRow().getIndex() + 1 + "");
                    } else {
                        setText("");
                    }
                }
            };
        }
    }
}
