package com.mobimore.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobimore.Configuration;
import com.mobimore.model.Cart;
import com.mobimore.model.ProductWrapper;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Product;
import com.mobimore.utils.FXUtils;
import com.mobimore.utils.HTTPUtils;
import com.mobimore.view.ProductView;
import com.victorlaerte.asynctask.AsyncTask;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainController {
    public Label itemsInShoppingCart;
    public ScrollPane contentScrollPane;
    public FlowPane contentFlowPane;
    public VBox progressIndicatorVBOX;
    private HTTPUtils httpUtils = HTTPUtils.getInstance();
    private Cart cart = new Cart();

    private ObservableList<Node> productViews = FXCollections.observableArrayList();

    public void initialize(){
        contentScrollPane.viewportBoundsProperty().addListener((bounds, oldBounds, newBounds) -> contentFlowPane.setPrefWidth(newBounds.getWidth()));
        productViews.addListener((InvalidationListener) e->{
            contentFlowPane.getChildren().clear();
            if (!productViews.isEmpty()) {
                contentFlowPane.getChildren().addAll(productViews);
            }
        });
        itemsInShoppingCart.textProperty().bind(cart.productsCountProperty().asString());
        new DownloadProductsTask().execute();
    }

    public void openShoppingCart(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(FXUtils.class.getResource("/fxml/shoppingCart.fxml"));
            Parent root = loader.load();
            ShoppingCartController controller = loader.getController();
            controller.setShoppingCart(cart);
            Stage stage = new Stage();
            stage.setTitle("Your shopping cart");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openMyProfile(ActionEvent actionEvent) {

    }

    private class DownloadProductsTask extends AsyncTask<String, Object, JSONResponse> {
        private Gson gson = new Gson();

        @Override
        public void onPreExecute() {
            //inputPane.setDisable(true);
            progressIndicatorVBOX.setVisible(true);
        }

        @Override
        public JSONResponse doInBackground(String[] params) {
            try {
                String url = "http://" + Configuration.SERVER_ADDRESS + ":" + Configuration.SERVER_PORT + "/getProducts";
                String urlParameters = "action=getAll";
                String response = HTTPUtils.getInstance().makeHTTPpostRequest(url, urlParameters);
                return gson.fromJson(response, JSONResponse.class);
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        public void onPostExecute(JSONResponse response) {
            progressIndicatorVBOX.setVisible(false);
            //inputPane.setDisable(false);
            if (response != null) {
                if (response.getErrorCode() == 0) {
                    String productsJSON = response.getResponseEntry("products");
                    if (productsJSON != null && !productsJSON.equals("")) {
                        Type type = new TypeToken<List<Product>>(){}.getType();
                        List<Product> products = gson.fromJson(productsJSON, type);
                        if (products != null) {
                            downloadProductsImages(products);
                            displayProducts(products);
                        }
                        return;
                    } else {
                        FXUtils.showAlert(Alert.AlertType.ERROR, "Error!", "Can't get products list");
                        return;
                    }
                }else {
                    FXUtils.showAlert(Alert.AlertType.ERROR, "Error!", "Can't get products list");
                    return;
                }
            }
            FXUtils.showAlert(Alert.AlertType.ERROR, "Error!", "Bad server response or server unavailable");
        }

        @Override
        public void progressCallback(Object[] objects) {

        }
    }

    private void downloadProductsImages(List<Product> products) {
        File cacheDirectory = new File(Configuration.IMAGES_CACHE);
        if (!cacheDirectory.isDirectory() || !cacheDirectory.exists()) {
            cacheDirectory.mkdirs();
        }
        for (Product product : products) {
            try {
                URL imageURL = new URL("http://" + Configuration.SERVER_ADDRESS + ":"+Configuration.SERVER_PORT+"/images/" + product.getType() + "/" + product.getImageName());
                BufferedImage image = ImageIO.read(imageURL);
                File typeDirectory = new File(cacheDirectory, product.getType());
                if (!typeDirectory.isDirectory() || !typeDirectory.exists()) {
                    typeDirectory.mkdirs();
                }
                ImageIO.write(image, "JPEG", new File(typeDirectory, product.getImageName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void displayProducts(List<Product> products) {
        List<ProductView> productViews = new ArrayList<>();
        for (Product product : products) {
            ProductView productView = new ProductView(product);
            ProductWrapper productWrapper = new ProductWrapper(product);
            productView.setAddToCartAction(e -> {
                cart.addToCart(productWrapper);
            });
            productViews.add(productView);
        }
        this.productViews.addAll(productViews);
    }
}
